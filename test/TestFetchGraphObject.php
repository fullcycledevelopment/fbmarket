<?php
require_once 'boot.php';

use FullCycle\FBMarket\FBObject;

\FullCycle\FBMarket\FBMarket::setApiVersion('v6.0');

if ($argc < 2) {
	echo "Must give order number\n";
	exit(0);
}

$id= $argv[1];
echo "testing fetch order $id\n";


$params = [
            'id'=>$id,
];

if ($argc == 3) {
	$params['fields'] = $argv[2];
}

 $fb_order = FBObject::create($params);

print_r($fb_order->__toArray(true));
echo "\n";



