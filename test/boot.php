<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . "/env.php";
require_once __DIR__ . "/Logger.php";

use FullCycle\FBMarket\FBMarket;


FBMarket::setApiPageId(env('FBMARKET_PAGE_ID'));
FBMarket::setApiAccessKey(env('FBMARKET_ACCESS_TOKEN'));

