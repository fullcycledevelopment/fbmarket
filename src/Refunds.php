<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;

/**
 * @author thrasher
 * 
 * @example
 *  RefundOrder::create([
 *      "order_id" => order_id_value,
 *      'items => [                 // Optional
 *          'retail_id' => xxxx-yyyy,
 *          'item_refund' => [
 *              'amount' => 5.00,
 *              'currency' => 'US',
 *          ],
 *          'shipping_refund' => [
 *              'amount' => 2.95,
 *              'currency' => 'US',
 *      ],
 *      reason_code=> RefundOrder::REFUND_REASON_OTHER,
 *      reason_text=> 'Text describing reason',
 * 
 *  ])
 * 
 */

class Refunds extends APIResource {
    protected $_request_url="refunds";
    protected $_method = "POST";
    
    const BUYERS_REMORSE="BUYERS_REMORSE";
    const DAMAGED_GOODS='DAMAGED_GOODS';
    const NOT_AS_DESCRIBED='NOT_AS_DESCRIBED';
    const QUALITY_ISSUE='QUALITY_ISSUE';
    const REFUND_REASON_OTHER='REFUND_REASON_OTHER';
    const WRONG_ITEM='WRONG_ITEM';
    
    function __construct($id = null, $opts = null) {
        if (isset($id['order_id'])) {
            $identifier = $id['order_id'];
            unset($id['order_id']);
        }
        parent::__construct($id,$opts);
        $this->_identifier= $identifier;
    }
    
    
}
