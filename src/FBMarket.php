<?php
namespace FullCycle\FBMarket;

class FBMarket {
	static protected $apiAccessKey = false;
	static protected $apiBase = 'https://graph.facebook.com';
	static protected $apiVersion = 'v18.0';
	static protected $apiPageId = null;
	
	function __construct($access_key=false) {
		$this->access_key = $access_key;
	}
	
	static public function getApiVersion() {
	    return self::$apiVersion;
	}

	static public function setApiVersion($version) {
		self::$apiVersion = $version;
	}
	
	static public function getApiPageId() {
	    return self::$apiPageId;
	}
	
	static public function setApiPageId($pageId) {
	    self::$apiPageId = $pageId;
	}
	
	static public function getApiAccessKey() {
	    return self::$apiAccessKey;
	}
	
	static public function setApiAccessKey($apiAccessKey) {
	    self::$apiAccessKey = $apiAccessKey;
	}
	
	static public function getApiBase() {
	    return self::$apiBase;
	}

}


