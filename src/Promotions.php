<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;

class Promotions extends APIResource {
    protected $_request_url="promotions";
    protected $_method = "GET";
    
    function __construct($id = null, $opts = null) {
        if (isset($id['order_id'])) {
            $identifier = $id['order_id'];
            unset($id['order_id']);
        }
        parent::__construct($id,$opts);
        $this->_identifier= $identifier;
    }
    
    function getOrderId() {
        return $this->_order_id;
    }
    
/*
    function makeUri() {
        $uri = parent::makeUri();
	echo "parent uri: $uri\n";
        $uri = "{$this->getApiBaseUrl()}/v6.0/{$this->getIdentifier()}/{$this->getRequestUrl()}";
//        $uri = "{$this->getApiBaseUrl()}/{$this->getOrderId()}/{$this->getRequestUrl()}";
//        print_r($this->_retrieveOptions);
//        echo "AttachShipment URI is: {$uri}\n";
//        die();
	echo "URI is: $uri\n";
        return $uri;
    }
*/   
    
}
