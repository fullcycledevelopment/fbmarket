<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;

/**
 * 
 * @author thrasher
 *
 * @example
 *  AcknowledgeOrders::create([
 *      'orders' = [
 *          ['id'=>id1],
 *          ['id'=>id2],
 *      ],
 *  ])
 *
 */

class AcknowledgeOrders extends APIResource {
    protected $_request_url="acknowledge_orders";
    protected $_method = "POST";
    
    function __construct($id = null, $opts = null) {
        parent::__construct($id,$opts);
        $this->setIdentifer(FBMarket::getApiPageId());
    }
        
}
