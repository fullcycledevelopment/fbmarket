<?php 

namespace FullCycle\FBMarket;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class APIRequestor {
    
    private $_apiKey;
    private $_apiBase;
    
    
    function __construct($_apiKey = null, $_apiBase = null) {
        $this->_apiKey = $_apiKey;
        $this->_apiBase = $_apiBase;
        
    }
    
    function request($method, $url, $params = null, $headers = null) {
        $params = $params ?: [];
        $headers = $headers ?: [];
        
        return $this->_requestRaw($method,$url,$params,$headers);
    }
    
    private function _requestRaw($method, $url, $params, $headers)
    {
        $this->client = new Client();
        $method = strtoupper($method);
        $myApiKey = $this->_apiKey;
        if (!$myApiKey) {
            $myApiKey = FBMarket::getApiAccessKey();
        }
        if (!$myApiKey) {
            throw new \Exception('Must set API Key');
        }
        $headers = $this->_defaultHeaders($myApiKey);
        $opts = [
            'headers'=>$headers,
        ];
        if ($method=="GET") {
            $opts['query'] = $params;
        }
        if ($method == "POST") {
            $opts['json'] = $params;
        }
        $resp = $this->client->request($method,$url,$opts);
        return $resp;
    }
    
    private function _defaultHeaders($apiKey) {
        $defaultHeaders = [
 //           'Token' => $apiKey,
            'Authorization' => "OAuth {$apiKey}",
        ];
        return $defaultHeaders;
        
    }
    
}

