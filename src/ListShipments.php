<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;

class ListShipments extends APIResource {
    protected $_request_url="shipments";
    protected $_method = "GET";
    
    function __construct($id = null, $opts = null) {
        if (isset($id['order_id'])) {
            $identifier = $id['order_id'];
            unset($id['order_id']);
        }
        parent::__construct($id,$opts);
        $this->_identifier= $identifier;
    }
    
    function getOrderId() {
        return $this->_order_id;
    }
    
}
