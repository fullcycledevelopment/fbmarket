<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;

class AttachShipment extends APIResource {
    protected $_request_url="shipments";
    protected $_method = "POST";
    
    function __construct($id = null, $opts = null) {
        if (isset($id['order_id'])) {
            $identifier = $id['order_id'];
            unset($id['order_id']);
        }
        parent::__construct($id,$opts);
        $this->_identifier= $identifier;
    }
    
    function getOrderId() {
        return $this->_order_id;
    }
    
/*
    function makeUri() {
        $uri = "{$this->getApiBaseUrl()}/{$this->getApiVersion()}/{$this->getOrderId()}/{$this->getRequestUrl()}";
//        print_r($this->_retrieveOptions);
//        echo "AttachShipment URI is: {$uri}\n";
//        die();
        return $uri;
    }
   */ 
    
}
