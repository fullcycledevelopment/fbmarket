<?php

namespace FullCycle\FBMarket\Util;

use FullCycle\FBMarket\FBMarketObject;

class Util {
    
    /**
     * Whether the provided array (or other) is a list rather than a dictionary.
     * A list is defined as an array for which all the keys are consecutive
     * integers starting at 0. Empty arrays are considered to be lists.
     *
     * @param array|mixed $array
     * @return boolean true if the given object is a list.
     */
    public static function isList($array)
    {
        if (!is_array($array)) {
            return false;
        }
        if ($array === []) {
            return true;
        }
        if (array_keys($array) !== range(0, count($array) - 1)) {
            return false;
        }
        return true;
    }
    
    /**
     * Converts a response from the FBMarket API to the corresponding PHP object.
     *
     * @param array $resp The response from the FBMarket API.
     * @param array $opts
     * @return FBMarketObject|array
     */
    public static function convertToFBMarketObject($resp, $opts)
    {
        $types = [
        ];
//        echo "converting to strip object\n";
//        print_r( $resp);
        if (self::isList($resp)) {
            $mapped = [];
            foreach ($resp as $i) {
                array_push($mapped, self::convertToFBMarketObject($i, $opts));
            }
            return $mapped;
        } elseif (is_array($resp)) {
            if (isset($resp['object']) && is_string($resp['object']) && isset($types[$resp['object']])) {
                $class = $types[$resp['object']];
            } else {
                $class = '\\FullCycle\\FBMarket\\FBMarketObject';
            }
            return $class::constructFrom($resp, $opts);
        } else {
            return $resp;
        }
    }
    
    /**
     * Recursively converts the PHP FBMarket object to an array.
     *
     * @param array $values The PHP FBMarket object to convert.
     * @return array
     */
    public static function convertFBMarketObjectToArray($values)
    {
        $results = [];
        foreach ($values as $k => $v) {
            // FIXME: this is an encapsulation violation
            if (is_array($k) && $k[0] == '_') {
                continue;
            }
            if ($v instanceof FBMarketObject) {
                $results[$k] = $v->__toArray(true);
            } elseif (is_array($v)) {
                $results[$k] = self::convertFBMarketObjectToArray($v);
            } else {
                $results[$k] = $v;
            }
        }
        return $results;
    }
    
    
}
