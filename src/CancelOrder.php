<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;
/**
 * 
 * 
 * @author thrasher
 *
 * @example 
 *      CancelOrder::create([
 *          "order_id" => order_id_value,
 *          "order_cancel_reason" => [
 *              "reason_code"=>CancelOrder::CANCEL_REASON_OTHER,
 *              "reason_description" => "Some text description",
 *          ]
 *           
 *      ]);
 *
 */
class CancelOrder extends APIResource {
    protected $_request_url="cancel_order";
    protected $_method = "POST";
    
    const CUSTOMER_REQUESTED='CUSTOMER_REQUESTED';
    const OUT_OF_STOCK='OUT_OF_STOCK';
    const INVALID_ADDRESS='INVALID_ADDRESS';
    const SUSPICIOUS_ORDER='SUSPICIOUS_ORDER';
    const CANCEL_REASON_OTHER='CANCEL_REASON_OTHER';
    
    function __construct($id = null, $opts = null) {
        if (isset($id['order_id'])) {
            $identifier = $id['order_id'];
            unset($id['order_id']);
        }
        parent::__construct($id,$opts);
        $this->_identifier= $identifier;
    }
    
    
}
