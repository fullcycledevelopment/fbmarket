<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;
use FullCycle\FBMarket\FBMarket;

class CommerceOrders extends APIResource {
    protected $_request_url="commerce_orders";
    protected $_method = "GET";
    
    function __construct($id = null, $opt = null) {
        parent::__construct($id, $opt);
        $this->setIdentifer(FBMarket::getApiPageId());
    }
    
}
