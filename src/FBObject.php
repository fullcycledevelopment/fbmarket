<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\APIResource;
use FullCycle\FBMarket\FBMarket;

class FBObject extends APIResource {
    protected $_request_url="";
    protected $_method = "GET";
    
    function __construct($id = null, $opt = null) {
	if (isset($id['id'])) {
		$_identifer = $id['id'];
		unset($id['id']);
	}
        parent::__construct($id, $opt);
        $this->setIdentifer($_identifer);
    }
    
}
