<?php 

namespace FullCycle\FBMarket;

use FullCycle\FBMarket\FBMarketObject;
use FullCycle\FBMarket\FBMarket;

class APIResource extends FBMarketObject {
    
    protected $_request_url=null;
    protected $_method = "GET";
    protected $_identifier = null;
    
    function __construct($id=null, $opts = null) {
        parent::__construct($id,$opts);
        
    }
    
    function getApiVersion() {
        return FBMarket::getApiVersion();
    }
    
    function getApiBaseUrl() {
        return FBMarket::getApiBase();
    }
    
    function getRequestUrl() {
        return $this->_request_url;
    }
    
    function setIdentifer($identifer) {
        $this->_identifier = $identifer;
    }
    
    function getIdentifier() {
        if (!$this->_identifier) {
            throw new \Exception("Must have an identifer set for URL  of form https://base/version/identifer/action.");
        }
        return $this->_identifier;
    }
    
    static public function create($id=null, $opts=null) {
        
        $instance = new static($id, $opts);
        $instance->refresh();
        return $instance;
    }
    
    function refresh() {
        $uri = $this->makeUri();
        $requestor= new \FullCycle\FBMarket\APIRequestor();
        $resp = $requestor->request($this->_method,$uri,$this->_retrieveOptions);
        $this->refreshFrom(json_decode($resp->getBody(),true));            
    }
    
    function makeUri() {
        $uri = "{$this->getApiBaseUrl()}/{$this->getApiVersion()}/{$this->getIdentifier()}/{$this->getRequestUrl()}";
        return $uri;
    }
    
    function next() {
        if ($this->paging && $this->paging->next) {
            $this->_retrieveOptions['after'] = $this->paging->cursors->after;
            $this->refresh();
            return $this;
        }
        return false;
    }
    
}

